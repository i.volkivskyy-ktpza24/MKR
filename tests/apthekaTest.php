<?php


use PHPUnit\Framework\TestCase;


class apthekaTest extends TestCase
{

    private $prod;

    public function setUp(): void
    {
        $this->prod = new aptheka();
    }


    public function testGetMedByName()
    {
        $this->assertSame('viagra', $this->prod->getMedByName('viagra'));
    }


    public function testGetCountMedCountry()
    {
        $this->assertSame(2, $this->prod->getCountMedCountry('Ukraine'));
    }

    public function testGetERRORCountMedCountry()
    {
        $this->assertSame(1, $this->prod->getCountMedCountry('Ukraine'));
    }


}
