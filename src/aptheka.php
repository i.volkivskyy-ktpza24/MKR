<?php


include 'medic.php';


class aptheka
{

    private $prod;
    public int $cnt;


    /**
     * aptheka constructor.
     */
    public  function __construct(){


        $this->prod =[
            new medic('paracetamol','pill','China'),
            new medic('viagra','pill','India'),
            new medic('smekta','dust','Russian Federation'),
            new medic('demedrol','candle','Ukraine'),
            new medic('saline blood','liquid','Ukraine'),
        ];
    }

    public function getMedByName($name){
        foreach ($this->prod as $med)
            if ($med->getName() == $name)
                return $med->getName();
        return null;
    }

    public function getCountMedCountry($cantry){
        $cnt=0;
        foreach ($this->prod as $med)
            if ($med->getCantry() == $cantry)
                $cnt= $cnt+1;
        return $cnt;
    }

    public function getMedByCountry($CT){
        foreach ($this->prod as $med)
            if ($med->getCantry() == $CT)
                return $med->getCantry();
        return null;
    }

}