<?php


class medic
{

    private $name;
    private $type;
    private $cantry;

    /**
     * medic constructor.
     * @param $name
     * @param $type
     * @param $cantry
     */
    public function __construct($name, $type, $cantry)
    {
        $this->name = $name;
        $this->type = $type;
        $this->cantry = $cantry;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCantry()
    {
        return $this->cantry;
    }

    /**
     * @param mixed $cantry
     */
    public function setCantry($cantry): void
    {
        $this->cantry = $cantry;
    }


}